$(document).ready(function() {
	
	$('.dotdotdot').dotdotdot();

	$('.goods-preview').zoom();

	$('.btn-call, .btn-buy').magnificPopup({
		type: 'inline'
	});

	$('.goods-content .more').click(function() {
		if ($(this).hasClass('opened')) {
			$(this).removeClass('opened').text('Читать описание полностью').closest('p').next('.content-more').slideUp();
		} else {
			$(this).addClass('opened').text('Свернуть описание').closest('p').next('.content-more').slideDown();
		}
		return false;
	});

	$('.goods-single').click(function() {
		if (!$(this).hasClass('active-img')) {
			var src = '';
			$(this).closest('.goods-list').find('.goods-single').removeClass('active-img');
			$(this).addClass('active-img');
			src = $(this).find('img').attr('src');
			src = src.slice(0, -7);
			$(this).closest('.container').find('.goods-preview img').attr('src', src + '.jpg');
			$(this).closest('.container').find('.goods-preview .zoomImg').remove();
		}
	});

	$('.delivery-type').click(function() {
		if ($(this).hasClass('delivery-yes')) {
			$('input[type=hidden].delivery').val(0);
			$(this).removeClass('delivery-yes').addClass('delivery-no').text('Без доставки');
			$(this).closest('.right').find('input').attr('disabled', 'disabled');
			$(this).closest('.right').find('input, label').slideUp();
		} else {
			$('input[type=hidden].delivery').val(1);
			$(this).removeClass('delivery-no').addClass('delivery-yes').text('С доставкой');
			$(this).closest('.right').find('input').removeAttr('disabled');
			$(this).closest('.right').find('input, label').slideDown();
		}
		return false;
	});

	$('.btn-buy').click(function() {
		$('input[type=hidden].order').val($(this).closest('.container').find('.goods-title').text());
	})

	$('#call, .form-block1').submit(function(e) {

        var name = $(this).children('.name').val();
        var phone = $(this).children('.phone').val();
        var email = $(this).children('.email').val();

        $.post('send.php', {
            name: name,
            phone: phone,
            email: email
        },
        function(data) {
            if (data === 'sended') {
                $.magnificPopup.open({
                	items: {
                		src: '#thanks',
                		type: 'inline'
                	}
                });
            } else {
                alert('Сообщение не отправлено');
            }
        });

        return false;

    });

    $('#buy').submit(function(e) {

        var name = $(this).find('.name').val();
        var phone = $(this).find('.phone').val();
        var email = $(this).find('.email').val();
        var order = $(this).find('.order').val();
        var delivery = $(this).find('.delivery').val();
        var region = $(this).find('.region').val();
        var address = $(this).find('.address').val();
        var comment = $(this).find('.comment').val();

        $.post('sendOrder.php', {
            name: name,
            phone: phone,
            email: email,
            delivery: delivery,
            order: order,
            address: address,
            region: region,
            comment: comment
        },
        function(data) {
            if (data === 'sended') {
                $.magnificPopup.open({
                	items: {
                		src: '#thanks',
                		type: 'inline'
                	}
                });
            } else {
                alert('Сообщение не отправлено');
            }
        });

        return false;

    });
});